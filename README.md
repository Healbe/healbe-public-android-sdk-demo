# HEALBE SDK Example
HEALBE SDK quickstart. Shows login, connection and data using process.

## Requirements

Minimum Android SDK is 24 (Android 7.0) since HEALBE SDK Android 1.5.0-beta01.

Target Android SDK is 32 (Android 12L) since HEALBE SDK Android 1.4.0.

## Dependencies
To use in your project add to your `build.gradle`:
 
```groovy
repositories { 
    maven { url "https://nexus.healbe.com/repository/maven-public/" }
}
```

And add SDK dependency:

```groovy
dependencies {
    implementation("com.healbe:healbesdk:1.5.2")
}
```

Also requires [API desugaring](https://developer.android.com/studio/write/java8-support#library-desugaring) dependency:

```groovy
dependencies {
    coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:2.0.2")
}
```

## Documentation

You can read or download javadoc [here](https://healbesdk.bitbucket.io)

## Examples
There are two examples:
* `simple_example` with only one Activity. It shows how to initialize SDK, connect to known GoBe and observe some data. You need to fill data in SimpleActivity's companion object for it to work.
* `complex_example` with simple dashboard for HEALBE SDK current values and wizards for login and connection.

Module `common` has some helper methods and common for both examples resources.

## Logs
HEALBE SDK use [Timber](https://github.com/JakeWharton/timber) to log its actions. To see it you need place timber log tree in plant: 

```kotlin
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}
```

## Versions changes
To see what's new in HEALBE SDK you can see [CHANGELOG.md](https://bitbucket.org/HEALBE/healbe-public-android-sdk-demo/src/master/CHANGELOG.md) in this repository.