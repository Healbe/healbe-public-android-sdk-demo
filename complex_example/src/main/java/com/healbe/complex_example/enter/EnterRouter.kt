package com.healbe.complex_example.enter

interface EnterRouter {
    fun goFillData()
    fun goLogin()
    fun goSignUp()
    fun goConnect()
    fun goSplash()
}