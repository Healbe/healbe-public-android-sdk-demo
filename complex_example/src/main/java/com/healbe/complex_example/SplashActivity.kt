package com.healbe.complex_example

import android.bluetooth.BluetoothAdapter
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import com.healbe.complex_example.connect.ConnectActivity
import com.healbe.complex_example.databinding.ActivitySplashBinding
import com.healbe.complex_example.enter.EnterActivity
import com.healbe.examples.common.BluetoothBroadcastReceiver
import com.healbe.healbesdk.business_api.HealbeSdk
import com.healbe.healbesdk.business_api.user.data.HealbeSessionState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

class SplashActivity : AppCompatActivity() {
    // for saving any opened subscribers in activity
    private val destroy = CompositeDisposable()

    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(LayoutInflater.from(this))
        with (binding) {
            setContentView(root)
            versionText.text = getString(R.string.sdk_example, BuildConfig.VERSION_NAME)
        }
        applicationContext.registerReceiver(
            BluetoothBroadcastReceiver,
            IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        )
        BluetoothBroadcastReceiver.init(applicationContext)
        initSdk()
    }

    // init sdk
    private fun initSdk() {
        // in big project recommended to move sdk operations in some abstract layer like presenters in mvp or something else
        // in example its ok, but remember our "destroy" disposable to support lifecycle
        val configuration = HealbeSdk.Configuration.Builder().setGroupMinutes(1).build()
        destroy.add(
            HealbeSdk.init(applicationContext, configuration)
                .subscribe(
                    // after SDK initialization we need to prepare user session
                    { initSession() },
                    //and we need to consume error in each call of sdk reactive methods
                    //when we don't know what to do with error - we log it with Timber
                    { t -> Timber.e(t) }
                )
        )
    }

    // session preparing
    private fun initSession() {
        destroy.add(
            HealbeSdk.get().USER.prepareSession()
                // all sdk rx methods work in own schedulers, so we need to listen them in UI thread for UI operations
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { sessionState: HealbeSessionState -> sessionStateSwitch(sessionState) },
                    { t -> Timber.e(t) }
                )
        )
    }

    private fun sessionStateSwitch(sessionState: HealbeSessionState) {
        when (sessionState) {
            // user profile is valid
            HealbeSessionState.VALID_OLD_USER,
            HealbeSessionState.VALID_NEW_USER -> goConnect()

            // there is no authorized user or some data is missing or invalid
            HealbeSessionState.USER_NOT_AUTHORIZED,
            HealbeSessionState.NEED_TO_FILL_PERSONAL,
            HealbeSessionState.NEED_TO_FILL_PARAMS -> goEnter()

            // SDK isn't ready to work, try to prepare session
            HealbeSessionState.SESSION_NOT_PREPARED -> initSession()
        }
    }

    private fun goEnter() {
        EnterActivity.start(this)
        finish()
    }

    private fun goConnect() {
        ConnectActivity.start(this)
        finish()
    }

    override fun onDestroy() {
        // clear all subscribes
        destroy.clear()
        super.onDestroy()
    }
}