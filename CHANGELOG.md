Change Log
==========

1.5.2
-----

Changes from 1.5.1.

**Behavior changes**

* Partially revert new behavior for current heart rate added in 1.2.0.
  * `Tasks.observeCurrentHeart()` remains removed.
  * `HealthData.observeHeartRate()` is still a way to get current Heart Rate.
  * `HealthData.observeHeartRate()` will only have valid value when we connected to GoBe and the GoBe is on a hand.
* Like for other `SampleData` types `HealthData.observeLastHydrationData()` will emit `HydrationData.EMPTY` if `HydrationData` has been received from a GoBe more than 10 minutes ago.

**Enhancement**

* `HDWeight` will be added on the first `User.updateUser()` with a valid profile or during synchronization of weight data with the server if there is no one. So `HealthData` weight methods will almost always return at least one `HDWeight`.
* Enable energy balance calibration. It will for with new GoBe3 firmwares only and if a user add weight-ins two-three times a weak and wears GoBe3 every day.

**Bugfix**

* Checking "on hand" status, battery level and current heart rate will only begin if we fully connect to a GoBe and it is ready to work. It should fix some issues with reconnection on disconnect and after installation of a firmware.
* Fix emitting a shallow copy of `HealbeUser` which leads to update of the profile on the server and a connected GoBe without `User.updateUser()` call.

**Other**

* On connection to a GoBe we now specify that BLE should be used (BluetoothDevice.TRANSPORT_LE). It should fix connection issues on some Android devices.
* Speed up session preparation by removing the wait for some requests to the server.

1.5.1
-----

Changes from 1.5.0.

**Enhancement**

* Add method `User.syncUserWithServer()` to immediately begin user synchronization with the server and a connected GoBe.
* Rewrite user profile synchronization to eagerly send data to the server and to a connected GoBe on user profile update.

**Bugfix**

* Fix synchronization with a GoBe3 if there were no synchronizations for a long time.
* Background user synchronizations seize to run if there was an error on requests to the server.

**Other**

* Update how SDK internally work with a non default `groupMinutes`. Prepare to be able to set it from the server.
* Fix calculation of new available firmwares count if there are several firmwares of the same type. And add check for new available firmwares after a firmware is updated.
* Update Android Gradle Plugin to 7.4.2, Gradle to 7.5 and Java toolchain to 11. Recommend to update desugaring library to 2.0.x.
* Update Kotlin to 1.8.22.
* Update SQLDelight to 1.5.5.
* Update androidx.annotations to 1.6.0.
* Update kotlinx.atomicfu to 0.21.0.
* Update kotlinx.coroutines to 1.7.3.
* Update kotlinx.serialization to 1.5.1.
* Update ktor to 2.3.3.

1.5.0
-----

Changes from 1.4.0.

**Important**

* Minimum Android SDK is increased to 24 (Android 7.0) from 21 (Android 5.0).

**API changes**

* Context parameter has been removed from method `AdditionalLogFilesStrategy.prepareFiles()`.
* Property `SleepPreferences.comfortableSleepDurationConfig` is not nullable anymore.

**Behavior changes**

* Remove user profile update on session preparation to speed up SDK initialization process on a weak Internet connection. Background user profile update will still kick in almost immediately.
* `HealbeDevice.equals()` begin to check other properties and not only `mac` to find out equality. For old behavior use new method `HealbeDevice.isSame()`.
* From now on `SensorState.timeToBatteryEnd` will always return 0 and will be removed in future releases. Predictions was too random and can not be trusted.
* Method `HealthData.getNeuroSummary()` calculated some data wrong. Implementation has been changed with call to `HealthData.neuro.observeNeuroSummary()`.

**Enhancement**

* Add property `HealbeUser.registrationTimestamp` with date and time of HEALBE account registration as UNIX-timestamp in UTC. Negative number if is is unknown.
* Add preliminary support for energy balance calibration. But it's disabled for now and will be enabled in future released when firmware is support the feature.

**Bugfix**

* Fix synchronization with a GoBe after relogin into another HEALBE account during the same HEALBE SDK initialization.
* Fix `NeuroSummary.getCalmnessGsrDuration()` result. Duration in the calculation state was included into the level duration.

**Deprecations**

* `SensorState.timeToBatteryEnd` has been deprecated. See Behavior changes above for more information.
* `HealthData.neuroDataFlow` is renamed into `HealthData.neuro` and former has been deprecated.
* Interface `NeuroDataFlow` is renamed into `Neuro` and former has been deprecated.
* Methods `HealthData.getMinStressData()` and `HealthData.getMaxStressData()` has been deprecated because data can change too rapidly during background synchronization to rely on one-time answer.
* Method `HealthData.getNeuroSummary()` has been deprecated. Use `HealthData.neuro.observeNeuroSummary()` instead.
* Method `HealthData.observeCurrentStressDuration()` has been deprecated. Duration should depend on the current timestamp but it updates now based on data update. And this method doesn't specify how long can be. So this method shouldn't be used and will be removed in next major release.
* Class `CurrentStressDuration` has been deprecated because the only public method returning this class has been deprecated.
* All methods for receiving of sleep data at `HealthData` interface has been deprecated.
* All `*Count` methods at `HealthData` interface has been deprecated because data can change too rapidly during background synchronization to rely on one-time answer.
* All `*Weekly*` fields at `HealthData` interface has been deprecated.
* Class `Average` has been deprecated. Use other implementations of `AverageSummary`.

**Other**

* Migrate from Room to SQLDelight (version 1.5.4).
* Internal improvements in network layer.
* A lot of `HealthData` has been migrated from RxJava2 to Kotlin Flow which will increase overhead due to conversion from Kotlin Flow to RxJava2 for now.
* Update Kotlin to 1.7.10.
* Update kotlinx.datetime to 0.4.0.
* Update kotlinx.ktor to 2.2.4. This is breaking change if you still use ktor 1.x.
* Update kotlinx.serialization to 1.4.1.
* Add kotlinx.atomicfu dependence of version 0.18.5.

1.5.0-beta02
------------

**Bugfix**

* Fix synchronization with a GoBe after relogin into another HEALBE account during the same HEALBE SDK initialization.

1.5.0-beta01
------------

**Important**

* Minimum Android SDK is increased to 24 (Android 7.0) from 21 (Android 5.0).

**API changes**

* Context parameter has been removed from method `AdditionalLogFilesStrategy.prepareFiles()`.
* Property `SleepPreferences.comfortableSleepDurationConfig` is not nullable anymore.

**Behavior changes**

* Remove user profile update on session preparation to speed up SDK initialization process on a weak Internet connection. Background user profile update will still kick in almost immediately.
* `HealbeDevice.equals()` begin to check other properties and not only `mac` to find out equality. For old behavior use new method `HealbeDevice.isSame()`.
* From now on `SensorState.timeToBatteryEnd` will always return 0 and will be removed in future releases. Predictions was too random and can not be trusted.
* Method `HealthData.getNeuroSummary()` calculated some data wrong. Implementation has been changed with call to `HealthData.neuro.observeNeuroSummary()`.

**Enhancement**

* Add property `HealbeUser.registrationTimestamp` with date and time of HEALBE account registration as UNIX-timestamp in UTC. Negative number if is is unknown.
* Add preliminary support for energy balance calibration. But it's disabled for now and will be enabled in future released when firmware is support the feature.

**Bugfix**

* Fix `NeuroSummary.getCalmnessGsrDuration()` result. Duration in the calculation state was included into the level duration.

**Deprecations**

* `SensorState.timeToBatteryEnd` has been deprecated. See Behavior changes above for more information.
* `HealthData.neuroDataFlow` is renamed into `HealthData.neuro` and former has been deprecated.
* Interface `NeuroDataFlow` is renamed into `Neuro` and former has been deprecated.
* Methods `HealthData.getMinStressData()` and `HealthData.getMaxStressData()` has been deprecated because data can change too rapidly during background synchronization to rely on one-time answer.
* Method `HealthData.getNeuroSummary()` has been deprecated. Use `HealthData.neuro.observeNeuroSummary()` instead.
* Method `HealthData.observeCurrentStressDuration()` has been deprecated. Duration should depend on the current timestamp but it updates now based on data update. And this method doesn't specify how long can be. So this method shouldn't be used and will be removed in next major release.
* Class `CurrentStressDuration` has been deprecated because the only public method returning this class has been deprecated.
* All methods for receiving of sleep data at `HealthData` interface has been deprecated.
* All `*Count` methods at `HealthData` interface has been deprecated because data can change too rapidly during background synchronization to rely on one-time answer.
* All `*Weekly*` fields at `HealthData` interface has been deprecated.
* Class `Average` has been deprecated. Use other implementations of `AverageSummary`.

**Other**

* Migrate from Room to SQLDelight (version 1.5.4).
* Internal improvements in network layer.
* A lot of `HealthData` has been migrated from RxJava2 to Kotlin Flow which will increase overhead due to conversion from Kotlin Flow to RxJava2 for now.
* Update Kotlin to 1.7.10.
* Update kotlinx.datetime to 0.4.0.
* Update kotlinx.ktor to 2.2.4. This is breaking change if you still use ktor 1.x.
* Update kotlinx.serialization to 1.4.1.
* Add kotlinx.atomicfu dependence of version 0.18.5.

1.4.0
-----

Changes from 1.3.2.

**API changes**

* Fix naming for new Neuroactivity classes and methods. `NeuroData`, `NeuroTrend`, `GspActivityLevel` and `NeuroSummary` have been moved from `com.healbe.healbesdk.business_api.healthdata.data.neuralctivity` into `com.healbe.healbesdk.business_api.healthdata.data.neuroactivity`. So we cannot add @Deprecation with ReplaceWith because these classes used as return value in interface `Neuro`.

**Documentation**

* Add more KDoc for Neuroactivity classes and methods.
* Fix some KDoc for Neuroactivity.

1.3.2
-----

Changes from 1.3.1.

**Other**

* Some changes to support internal researches.

1.4.0-beta01
------------

**API changes**

* Fix naming for new Neuroactivity classes and methods. `NeuroData`, `NeuroTrend`, `GspActivityLevel` and `NeuroSummary` have been moved from `com.healbe.healbesdk.business_api.healthdata.data.neuralctivity` into `com.healbe.healbesdk.business_api.healthdata.data.neuroactivity`. So we cannot add @Deprecation with ReplaceWith because these classes used as return value in interface `Neuro`.

**Documentation**

* Add more KDoc for Neuroactivity classes and methods.
* Fix some KDoc for Neuroactivity.

1.3.2-beta01
------------

**Other**

* Some changes to support internal researches.

1.3.2-alpha02
-------------

**Other**

* Some changes to support internal researches.

1.3.2-alpha01
-------------

**Other**

* Some changes to support internal researches.

1.3.1
-----

Changes from 1.3.0.

**New features**

* Add new data types: Neuroactivity. New data can be retrieved with `HealthData.neuro` entry point.
  * `NeuroData` is 5-minute sample with a level of nervous system (NS) activity.
  * NS activity falls into four states `GsrActivityLevel`. The more NS activity, the more state: from Calmness to Overload.
  * `NeuroSummary` shows summary for the day and night.
  * Nervous system works differently during sleep, so there will be no data (`NeroData`) during sleep.
* Add new property `BloodPressure.heartRate` with heart rate during blood pressure measurement.

**Other**

* Add some logs into synchronization process.

1.3.1-rc01
----------

**Enhancement**

* Add new data types: Neuroactivity. New data can be retrieved with `HealthData.neuro` entry point.
  * `NeuroData` is 5-minute sample with a level of nervous system (NS) activity.
  * NS activity falls into four states `GsrActivityLevel`. The more NS activity, the more state: from Calmness to Overload.
  * `NeuroSummary` shows summary for the day and night.
  * Nervous system works differently during sleep, so there will be no data (`NeroData`) during sleep.

1.3.1-beta01
-------------

**Other**

* Some internal fixes.

1.3.1-alpha03
-------------

**Other**

* Add some logs into synchronization process.

1.3.1-alpha02
-------------

**Other**

* Some internal fixes.

1.3.1-alpha01
-------------

**New features**

* Add new property `BloodPressure.heartRate` with heart rate during blood pressure measurement.

1.3.0
-----

Changes from 1.2.2.

**Important**

* Android SDK target is bumped to 32 (Android 12L).
* Minimum Android SDK is increased to 21 (Android 5.0).

**API changes**

* Deprecated `EnergySummary` properties `fat`, `carbohydrates` and `protein' have been removed.
* Deprecated `HydrationSummary` method `getTotalWaterLoss()` and property `totalWaterLoss` have been removed.
* Deprecated method `Tasks.observeHeartRate()` has been removed.
* Deprecated enum value `SyncState.COUNTING_SENSOR_HOLES` has been removed.

**Behavior changes**

* For Android 12 and above (Android SDK 31 and above) need to check for new Bluetooth runtime permissions (https://developer.android.com/about/versions/12/features/bluetooth-permissions).
* `GoBe.connect()` ignores invoke even if GoBe isn't currently connected but reconnection loop has been started previously with the method. Previously reconnection loop was recreated if GoBe isn't really connected at the time.

**Enhancement**

* Increase weight max value to 200 kg.
* Add methods `Tasks.isFastSyncEnabled()` and `Tasks.setFastSync()` to disabled faster synchronization with GoBe approach. It is enabled (and has been since 1.0.0) by default.
* Remove some unnecessary commands to smart band on user update if for smart band nothing is changed.

**Bugfix**

* Fix sending to a smart band some messed up units (eg. `WeightUnits`) that leads to unnecessary vibrations during connection.
* Add workaround to fix finding GoBe3 on Android 13 (Android SDK 33). GoBe3 can be found during scan but name generated by SDK.
* Fix possible `StressData.isStrongEmoTension == true` for some new GoBe3 firmwares though this field is deprecated for GoBe3.

**Other**

* Update Kotlin to 1.7.0.
* Update kotlinx.serialization to 1.4.0.
* Update androidx annotations to 1.5.0.
* Some changes to support internal researches.

1.3.0-rc01
----------

**Other**

* Some minor codebase cleanup after increasing Minimum Android SDK.

1.3.0-beta01
-------------

**Enhancement**

* Remove some unnecessary commands to smart band on user update if for smart band nothing is changed.

**Bugfix**

* Fix possible `StressData.isStrongEmoTension == true` for some new GoBe3 firmwares though this field is deprecated for GoBe3.

**Other**

* Some changes to support internal researches.

1.3.0-alpha01
-------------

**Important**

* Android SDK target is bumped to 32 (Android 12L).
* Minimum Android SDK is increased to 21 (Android 5.0).

**API changes**

* Deprecated `EnergySummary` properties `fat`, `carbohydrates` and `protein' have been removed.
* Deprecated `HydrationSummary` method `getTotalWaterLoss()` and property `totalWaterLoss` have been removed.
* Deprecated method `Tasks.observeHeartRate()` has been removed.
* Deprecated enum value `SyncState.COUNTING_SENSOR_HOLES` has been removed.

**Behavior changes**

* For Android 12 and above (Android SDK 31 and above) need to check for new Bluetooth runtime permissions (https://developer.android.com/about/versions/12/features/bluetooth-permissions).
* `GoBe.connect()` ignores invoke even if GoBe isn't currently connected but reconnection loop has been started previously with the method. Previously reconnection loop was recreated if GoBe isn't really connected at the time.

**Enhancement**

* Increase weight max value to 200 kg.
* Add methods `Tasks.isFastSyncEnabled()` and `Tasks.setFastSync()` to disabled faster synchronization with GoBe approach. It is enabled (and has been since 1.0.0) by default.

**Bugfix**

* Fix sending to a smart band some messed up units (eg. `WeightUnits`) that leads to unnecessary vibrations during connection.
* Add workaround to fix finding GoBe3 on Android 13 (Android SDK 33). GoBe3 can be found during scan but name generated by SDK.

**Other**

* Update Kotlin to 1.7.0.
* Update kotlinx.serialization to 1.4.0.
* Update androidx annotations to 1.5.0.

1.2.2
-----

Changes from 1.2.1.

**Bugfix**

* Try to fix reconnect loop after applying work around of bug in Android 12 (https://issuetracker.google.com/issues/207323675).
* Fix saving data from a GoBe in a rare case when there are already some of the data in data base. This issue was introduced in 1.2.1-alpha01.

1.2.2-rc02
----------

**Bugfix**

* Try to fix reconnect loop after applying work around of bug in Android 12 (https://issuetracker.google.com/issues/207323675).

1.2.2-rc01
----------

**Bugfix**

* Fix saving data from a GoBe in a rare case when there are already some of the data in data base. This issue was introduced in 1.2.1-alpha01.

1.2.1
-----

Changes from 1.2.0.

**Enhancement**

* Speed up synchronization by skip downloading of `AnxietyData` if it can be downloaded as part of another GoBe structure.
* Add `accumulatedStressMinutes` field into `StressData` samples. This value received from the smart band instead of calculation in SDK.
* `StressSummary.getAccumulatedStress()` becomes more accurate for new firmwares.

**Bugfix**

* Try to work around bug in Android 12 (https://issuetracker.google.com/issues/207323675) that cause SDK not to reconnect on disconnects.
* Add lost saving private data of sleep module into profile.
* At `HydrationSummary` methods `getNormalHydrationDuration()` and `getDehydrationDuration()` now returns result in specified `TimeUnit` (previously value was increased 60 times).
* Fix parsing private data of sleep module during connection.

**Other**

* Update kotlinx.coroutines to 1.6.4.
* Update Ktor Client to 1.6.8.
* Some changes to support internal researches.

1.2.1-beta03
-------------

**Other**

* Some fixes to support internal researches.

1.2.1-beta02
-------------

**Important**

* Database version has been decreased after increase in 1.2.1-beta01 due to unnecessary migration. So SDK won't init correctly for existing builds based on 1.2.1-beta01 SDK.

**Bugfix**

* Fix parsing private data of sleep module during connection.

**Other**

* Some fixes to support internal researches.

1.2.1-beta01
-------------

**Other**

* Some fixes to support internal researches.
* Update Ktor Client to 1.6.8.

1.2.1-alpha05
-------------

**Bugfix**

* At `HydrationSummary` methods `getNormalHydrationDuration()` and `getDehydrationDuration()` now returns result in specified `TimeUnit` (previously value was increased 60 times).

1.2.1-alpha04
-------------

**Enhancement**

* Add `accumulatedStressMinutes` field into `StressData` samples. This value received from the smart band instead of calculation in SDK.
* `StressSummary.getAccumulatedStress()` becomes more accurate for new firmwares.

**Bugfix**

* Add lost saving private data of sleep module into profile.

**Other**

* Some fixes to support internal researches.

1.2.1-alpha03
-------------

**Other**

* Some fixes to support internal researches.

1.2.1-alpha02
-------------

**Other**

* Some changes to support internal researches.
* Update kotlinx.coroutines to 1.6.4.

1.2.1-alpha01
-------------

**Enhancement**

* Speed up synchronization by skip downloading of AnxietyData if it can be downloaded as part of another GoBe structure.

**Bugfix**

* Try to work around bug in Android 12 (https://issuetracker.google.com/issues/207323675) that cause SDK not to reconnect on disconnects.

**Other**

* Some changes to support internal researches.

1.2.0
-----

Changes from 1.1.3.

**Important**

* Unfortunately Java 8 desugaring doesn't work for libraries. So you need to enable it to fix exceptions at HEALBE SDK 1.2.0-alpha04 and above. See https://developer.android.com/studio/write/java8-support

**New features**

* Add method `User.hasEmailConfirmed()` to check has user confirmed email.
* Add method `User.resendEmailConfirmation()` to resend confirmation email.

**Deprecations**

* `Tasks.observeHeartRate()` has been deprecated. Method has been moved into `HealthData` (see below).

**API changes**

* `KeyValueStorage`, `SharedPrefsStorage` and `ServerWrongCodeException` have been moved into the other packages. If `KeyValueStorage` and/or `SharedPrefsStorage` has been used, kotlinx.serialization needed to be added to the project and annotation `@Serializable` needed to be added to classes passed into `JsonParser`.
* Class `GsonParser` has been removed. User `JsonParser` instead.
* Old sleep deprecated methods and class `AwakeInfo` have been removed.
* Deprecated *Count and minWeekly*/maxWeekly* methods and properties at `HealthData` have been removed
* Deprecated extension-property `FirmwareUpdateState.inactive` has been removed.
* Deprecated class `Average` and enum values `AverageType.ASLEEP_LAST_NIGHT` and `AverageType.ASLEEP_TONIGHT` have been removed.
* Deprecated extension-functions `daysFromNight()`, `nightStart()` and `nightEnd()` have been removed.
* Deprecated enum values `ClientState.RESTARTING` and `ClientState.REQUEST_USER_INFO` have been removed.

**Behavior changes**

* Add check that `HealbeUser.gender` is valid during session preparation.

**Enhancement**

* `AverageSummary.min` and `AverageSummary.max` calculates from minimum and maximum values from newer firmwares instead of average for five minute values.
* New approach for recent heart rate value:
  ** Add new class `CurrentHeartRate` as replace for `HeartRate`.
  *** Property `CurrentHeartRate.isValid` can be used to check is value still valid.
  *** Property `CurrentHeartRate.relevanceDurationInSeconds` specify how long value can be used as current.
  ** Add new method `HealthData.observeHeartRate()` to observe current Heart Rate.
  ** From now on there will be current heart rate event without connected smart band if there was synchronization recently.
  ** If current heart rate unknown, null-value `HeartRateUnknown` will be returned as always not valid value.
* Add new `HealbeSdk.Configuration` option to specify Bluetooth transport (see `BluetoothDevice.TRANSPORT_LE`). `BluetoothDevice.TRANSPORT_AUTO` is default.
* Add new user property `allowMarketingCommunication` (at `HealbeUser.UserPreferences`) to specify user desire to receive newsletters from HEALBE.

**Bugfix**

* Fix check for user state before rx-pipeline has been subscribed at some methods of `GoBe` interface.
* Fix always empty `Sleeps` for days outside of `SleepCacheConfiguration.cachedDaysCount downTo SleepCacheConfiguration.lastCachedDaysBack` days ago.
* Add logout if server logouts user after SDK initialization.
  Fix session state stays `SESSION_NOT_PREPARED` if user has been logged out on the server.
* Fix continuation of logs sending if progress subject has been unsubscribed from.

**Other**

* Gson and Retrofit2 dependencies has been replaced with kotlinx.serialization 1.3.3 and Ktor Client 1.6.7.
* Update Kotlin to 1.6.21
* Update Timber to 5.0.1
* Remove some undocumented and unused extension-functions from `com.healbe.healbesdk.utils` package.
* Remove some excess logging.
* Simplify synchronization based on features supported by firmware on the smart band.

1.2.0-beta04
------------

**Other**

* Remove some undocumented and unused extension-functions from `com.healbe.healbesdk.utils` package.

1.2.0-beta03
-------------

**Enhancement**

* Add new `HealbeSdk.Configuration` option to specify Bluetooth transport (see `BluetoothDevice.TRANSPORT_LE`). `BluetoothDevice.TRANSPORT_AUTO` is default.

1.2.0-beta02
-------------

**Important**

* Unfortunately Java 8 desugaring doesn't work for libraries. So you need to enable it to fix exceptions at HEALBE SDK 1.2.0-alpha04 and above. See https://developer.android.com/studio/write/java8-support

**Other**

* Remove some excess logging.

1.2.0-beta01
-------------

**Important**

* Unfortunately Java 8 desugaring doesn't work for libraries. So you need to enable it to fix exceptions at HEALBE SDK 1.2.0-alpha04 and above. See https://developer.android.com/studio/write/java8-support

**Bugfix**

* Fix some remaining issues with logs uploading progress.

1.2.0-alpha05
-------------

**Enhancement**

* Add new user property `allowMarketingCommunication` (at `HealbeUser.UserPreferences`) to specify user desire to receive newsletters from HEALBE.

**Bugfix**

* Fix logs uploading progress.
* Fix very slow firmware downloading.
* Fix exception in the Server API on Android SDK less than 26.
* Fix saving information about user confirmation of their email.

1.2.0-alpha04
-------------

**Deprecations**

* `Tasks.observeHeartRate()` has been deprecated. Method has been moved into `HealthData` (see below).

**API changes**

* `KeyValueStorage`, `SharedPrefsStorage` and `ServerWrongCodeException` have been moved into the other packages. If `KeyValueStorage` and/or `SharedPrefsStorage` has been used, kotlinx.serialization needed to be added to the project and annotation `@Serializable` needed to be added to classes passed into `JsonParser`.
* Class `GsonParser` has been removed. User `JsonParser` instead.
* Old sleep deprecated methods and class `AwakeInfo` have been removed.
* Deprecated *Count and minWeekly*/maxWeekly* methods and properties at `HealthData` have been removed
* Deprecated extension-property `FirmwareUpdateState.inactive` has been removed.
* Deprecated class `Average` and enum values `AverageType.ASLEEP_LAST_NIGHT` and `AverageType.ASLEEP_TONIGHT` have been removed.
* Deprecated extension-functions `daysFromNight()`, `nightStart()` and `nightEnd()` have been removed.
* Deprecated enum values `ClientState.RESTARTING` and `ClientState.REQUEST_USER_INFO` have been removed.

**Enhancement**

* Add new class `CurrentHeartRate` as replace for `HeartRate`.
  ** Property `CurrentHeartRate.isValid` can be used to check is value still valid.
  ** Property `CurrentHeartRate.relevanceDurationInSeconds` specify how long value can be used as current.
* Add new method `HealthData.observeHeartRate()` to observe current Heart Rate.
* From now on there will be current heart rate event without connected smart band if there was synchronization recently.
* If current heart rate unknown, null-value `HeartRateUnknown` will be returned as always not valid value.

**Bugfix**

* Fix retrieval of minimum and maximum pulse for `HealthData`.
* Fix check for user state before rx-pipeline has been subscribed at some methods of `GoBe` interface.
* Fix always empty `Sleeps` for days outside of `SleepCacheConfiguration.cachedDaysCount downTo SleepCacheConfiguration.lastCachedDaysBack` days ago.
* Add logout if server partially logout user.

**Other**

* Gson and Retrofit2 dependencies has been replaced with kotlinx.serialization 1.3.3 and Ktor Client 1.6.7.
* Update Kotlin to 1.6.21
* Update Timber to 5.0.1

1.2.0-alpha03
-------------

**Bugfix**

* Fix check of `HealbeUser.gender` during session preparation.

1.2.0-alpha02
-------------

**New features**

* Add method `User.hasEmailConfirmed()` to check has user confirmed email.
* Add method `User.resendEmailConfirmation()` to resend confirmation email.

1.2.0-alpha01
-------------

**Behavior changes**

* Add check that `HealbeUser.gender` is valid during session preparation.

**Enhancement**

* `AverageSummary.min` and `AverageSummary.max` calculates from minimum and maximum values from newer firmwares instead of average for five minute values.

**Bugfix**

* Fix session state stays `SESSION_NOT_PREPARED` if user has been logged out on the server.
* Fix continuation of logs sending if progress subject has been unsubscribed from.

**Other**

* Simplify synchronization based on features supported by firmware on the smart band.

1.1.3
-----

Changes from 1.1.2.

**Deprecations**

* `List<StressData>.toAccumulatedStress()` from `AccumulatedStressUtil` has been deprecated as less precise method to calculation accumulated stress for a day. If it is important to you, please reach out and tell as your use case.
* Properties `fat`, `carbohydrates` and `protein` at `EnergySummary` has been deprecated. Use new property `nutrients` instead.

**Behavior changes**

* Remove dot as valid symbol for first and last names.

**Enhancement**

* Add `StressSummary.getAccumulatedStress(units: TimeUnit = TimeUnit.SECONDS)` method with more precise calculation of accumulated stress.
* Add list of `StressData` into `StressSummary` to simplify observing stress data and decrease CPU and RAM usage if you need both.
* Separate summary about nutrients consumed during a day from `EnergySummary`:
  ** Add new class Nutrients.
  ** Add new method `HEALTH_DATA.observeNutrients(daysBack: Int): Flowable<Nutrients>` to observe nutrients consumed during the day `daysBack` ago without requesting whole `EnergySummary`.
  ** Add new property `nutrients: Nutrients` to `EnergySummary`.

**Bugfix**

* Another fix of `EnergyData.energyOut` value for records created right before the downloading from a smart band.
* Fix connection to GoBe1 and GoBe2 with very old firmwares.

1.1.3-alpha02
-------------

**Behavior changes**

* Remove dot as valid symbol for first and last names.

**Bugfix**

* Another fix of `EnergyData.energyOut` value for records created right before the downloading from a smart band.

1.1.3-alpha01
-------------

**Deprecations**

* `List<StressData>.toAccumulatedStress()` from `AccumulatedStressUtil` has been deprecated as less percice method to caluclation accumulated stress for a day. If it is important to you, please reach out and tell as your use case.
* Properties `fat`, `carbohydrates` and `protein` at `EnergySummary` has been deprecated. Use new property `nutrients` instead.

**Enhancement**

* Add `StressSummary.getAccumulatedStress(units: TimeUnit = TimeUnit.SECONDS)` method with more precise calculation of accumulated stress.
* Add list of `StressData` into `StressSummary` to simplify observing stress data and decrease CPU and RAM usage if you need both.
* Separate summary about nutrients consumed during a day from `EnergySummary`:
  ** Add new class Nutrients.
  ** Add new method `HEALTH_DATA.observeNutrients(daysBack: Int): Flowable<Nutrients>` to observe nutrients consumed during the day `daysBack` ago without requesting whole `EnergySummary`.
  ** Add new property `nutrients: Nutrients` to `EnergySummary`.

**Bugfix**

* Fix connection to GoBe1 and GoBe2 with very old firmwares.

1.1.2
-----

Changes from 1.1.1.

**New features**

* Add new methods at `User` interface to store and send to HEALBE server tokens. For more information see documentation for `User` interface and `Token` class.

**API changes**

* Add new more precise property `HydrationSummary.waterLoss: Float?` and method `HydrationSummary.getWaterLoss(): Float?` to use instead of `HydrationSummary.totalWaterLoss: Int?` and `HydrationSummary.getTotalWaterLoss(): Int?`.

**Deprecations**

* `HydrationSummary.totalWaterLoss: Int?` and `HydrationSummary.getTotalWaterLoss(): Int?` has been deprecated.

**Behavior changes**

* `Tasks.observeSyncState()` won't return `SyncState.COUNTING_SENSOR_HOLES` any more.

**Bugfix**

* Fix calculation of sleeps and anxieties on synchronization when the last sleep hasn't been finished yet.
* Fix calculation of sleep duration at `SleepSummary.getSleepDuration()` with units other than seconds when there are more than one sleep.

**Other**

* Remove some unused code in synchronization.

1.1.2-alpha06
-------------

**Behavior changes**

* `Tasks.observeSyncState()` won't return `SyncState.COUNTING_SENSOR_HOLES` any more.

**Other**

* Remove some unused code in synchronization.

1.1.2-alpha05
_____________

**Bugfix**

* Fix calculation of sleep and the day it should end if one hasn't ended yet, when anxieties changes during synchronization.

1.1.2-alpha04
_____________

**Behavior changes**

* Add sending tokens to the server on each change but not only during sdk initialization.

1.1.2-alpha03
_____________

**Bugfix**

* Fix calculation of sleeps and anxieties on synchronization when the last sleep hasn't been finished yet.
* Fix calculation of sleep duration at `SleepSummary.getSleepDuration()` with units other than seconds when there are more than one sleep.

1.1.2-alpha02
_____________

**New features**

* Add new methods at `User` interface to store and send to HEALBE server tokens. For more information see documentation for `User` interface and `Token` class.

1.1.2-alpha01
_____________

**API changes**

* Add new more precise property `HydrationSummary.waterLoss: Float?` and method `HydrationSummary.getWaterLoss(): Float?` to use instead of `HydrationSummary.totalWaterLoss: Int?` and `HydrationSummary.getTotalWaterLoss(): Int?`.
* `HydrationSummary.totalWaterLoss: Int?` and `HydrationSummary.getTotalWaterLoss(): Int?` has been deprecated.

1.1.1
_____

Changes from 1.1.0.

**New features**

* Add minimal support for new firmwares that provide data about water loss in millilitres (see more information in `HydrationData` and `HydrationSummary` documentation).
* Add uploading and downloading data from new firmware but some new data can be `null` after downloading.

**Enhancement**

* Add list of `HydrationData` into `HydrationSummary` to simplify observing hydration data and decrease CPU and RAM usage.
* Add `convertTo` and `invertFrom` with `VolumeUnits` parameter extension-methods for `Float`.

**Bugfix**

* Fix `EnergyData.energyOut` value for records created right before the downloading from a smart band.
* Fix rare fails to check are there new firmwares due to `null` name in the Android Bluetooth cache.

**Other**

* Update Kotlin to 1.5.31

1.1.1-rc03
----------

**Bugfix**

* Fix synchronization of new firmware data with HEALBE SDK iOs.

1.1.1-rc02
------------

**Enhancement**

* Add list of `HydrationData` into `HydrationSummary` to simplify observing hydration data and decrease CPU and RAM usage.

**Bugfix**

* Fix calculation of hydration duration and water loss for `HydrationSummary`.

1.1.1-rc01
------------

**New features**

* Add uploading and downloading data from new firmware but some new data can be `null` after downloading.

1.1.1-beta01
------------

**Behavior changes**

* `HealthDataImplementation.observeHydrationData()` returns `HydrationData` that starts within the day. Previous the method could return `HydrationData` that falls on midnight into both adjacent days.

**Bugfix**

* Fix parsing data for water loss if `groupSize` more than 7.
* Fix `HydrationSummary` data calculation due to samples that falls on midnight goes into both days.

**Other**

* Update Kotlin to 1.5.31

1.1.1-alpha05
-------------

**API changes**

* Type of properties `metabolicWaterLoss`, `activityWaterLoss` and returned type of methods `getMetabolicWaterLoss()`, `getActivityWaterLoss()` at `HydrationData` changed from `Int?` to `Float?`.

**Enhancement**

* Add `convertTo` and `invertFrom` with `VolumeUnits` parameter extension-methods for `Float`.

1.1.1-alpha04
-------------

**Bugfix**

* Fix data base migration for new water loss feature on Android 10 and lower.

1.1.1-alpha03
-------------

Combine changes from 1.1.1-alpha01 and 1.1.1-alpha02.

1.1.1-alpha02
-------------

**Bugfix**

* Fix `EnergyData.energyOut` value for records created right before the downloading from a smart band.
* Fix rare fails to check are there new firmwares due to `null` name in the Android Bluetooth cache.

1.1.1-alpha01
-------------

**New features**

* Add minimal support for new firmwares that provide data about water loss in millilitres (see more information in `HydrationData` and `HydrationSummary` documentation).

1.1.0
-----

No changes from 1.1.0-rc02.

**New features**

* Add support for new firmwares that gathers sleep data for a whole day and night (see more information in `SleepData` documentation).
* Add caching of sleep data to get data more quickly and decrease work of garbage collector (see more information in `SleepDataConfiguration` documentation). It is disabled by default.

**API changes**

* All methods to get sleep data has been moved into separate interface `SleepData`. It can be received via extension method `HealthData.sleepData`.
* Deprecated field `HealbeSdk.ARCHIVE` has been removed. Use `HealbeSdk.HEALTH_DATA` instead.
* Deprecated methods at `HealthData` has been removed.

**Deprecations**

* All methods for receiving of sleep data at `HealthData` interface has been deprecated.
* All `*Count` methods at `HealthData` interface has been deprecated because data can change too rapidly during background synchronization to rely on one-time answer.
* All `*Weekly*` fields at `HealthData` interface has been deprecated.
* Class `Average` has been deprecated. Use other implementations of `AverageSummary`.

**Behavior changes**

* `HeartSummary.averages` returns implementations of `AverageSummary` and do not return deprecated `Average` class any more.

**Enhancement**

* Add skip of empty generic data to decrease time of synchronization with a smart band when there are no new sleep data.
* Add timeout if firmware update has got stuck at `FirmwareUpdateState.FW_FLASH` for too long.

**Bugfix**

* Heart rate samples that fall at midnight includes in average data of the later day instead of both days.
* Fix parsing of energyOut data for some firmwares.

**Other**

* Update Kotlin to 1.5.20
* Update Gson to 2.8.7
* Update Room to 2.2.6
* Update RxJava to 2.2.21

1.1.0-rc02
----------

**Enhancement**

* Add timeout if firmware update has got stuck at `FirmwareUpdateState.FW_FLASH` for too long.

**Bugfix**

* Fix background check for firmwares updates fails if Android doesn't remember name of default smart band.

1.1.0-rc01
----------

**New features**

* Add support for new firmwares that gathers sleep data for a whole day and night (see more information in `SleepData` documentation).
* Add caching of sleep data to get data more quickly and decrease work of garbage collector (see more information in `SleepDataConfiguration` documentation). It is disabled by default.

**API changes**

* All methods to get sleep data has been moved into separate interface `SleepData`. It can be received via extension method `HealthData.sleepData`.
* Deprecated field `HealbeSdk.ARCHIVE` has been removed. Use `HealbeSdk.HEALTH_DATA` instead.
* Deprecated methods at `HealthData` has been removed.

**Deprecations**

* All methods for receiving of sleep data at `HealthData` interface has been deprecated.
* All `*Count` methods at `HealthData` interface has been deprecated because data can change too rapidly during background synchronization to rely on one-time answer.
* All `*Weekly*` fields at `HealthData` interface has been deprecated.
* Class `Average` has been deprecated. Use other implementations of `AverageSummary`.

**Behavior changes**

* `HeartSummary.averages` returns implementations of `AverageSummary` and do not return deprecated `Average` class any more.

**Enhancement**

* Add skip of empty generic data to decrease time of synchronization with a smart band when there are no new sleep data.

**Bugfix**

* Heart rate samples that fall at midnight includes in average data of the later day instead of both days.
* Fix parsing of energyOut data for some firmwares.

**Other**

* Update Kotlin to 1.5.20
* Update Gson to 2.8.7
* Update Room to 2.2.6
* Update RxJava to 2.2.21

1.0.10
------

**Behavior changes**

* remove skipping incomplete groups of data (skipping last data on the smart band if there are less than `groupSize` records)
* return back 30-second timeout to connect to the server
* decrease duration of synchronization with a smart band due to skip sleep data when there is no sleep data
* fix too frequent synchronization with a smart band
* add name of a smart band updating during connection
* fix emitting exceptions instead of `FW_NOT_AVAILABLE` if there are no firmwares for the smart band
* fix emit of new `SleepRecommendations` if one hasn't actually changed

**Enhancement**

* improve checks for availability of Internet connection at `ConnectionTool`

**Bugfix**

* fix avatar update
* fix skipping user weight update if there was error during non-first downloading of weights from the server
* fix some exceptions if we haven't notice disconnect during early stages of connection
* fix skipping one packet during continuation of firmware uploading after disconnect (caused to re-upload some already uploaded part of firmware)
* fix calculation of nutrients for meals split by midnight
* fix stuck at the flashing step of firmware update for GoBe3
* fix removing non-finalized meals on sync after a smart band reset

**Documentation**

* add documentation for ConnectionTool
* fix documentation for `GoBe.setPin()`: exception emitted on wrong connection state is `IllegalStateException`

**Logging**

* return back some logging at interactions with a smart band
* add more logs to firmware uploading process
* improve logging of synchronization
