package com.healbe.simple_example

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import androidx.activity.ComponentActivity
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.healbe.examples.common.Bluetooth
import com.healbe.examples.common.getCurrentLocale
import com.healbe.healbesdk.business_api.HealbeSdk
import com.healbe.healbesdk.business_api.healthdata.Container
import com.healbe.healbesdk.business_api.healthdata.data.heart.HeartData
import com.healbe.healbesdk.business_api.healthdata.data.stress.StressSummary
import com.healbe.healbesdk.business_api.healthdata.data.water.HydrationSummary
import com.healbe.healbesdk.business_api.tasks.entity.TasksState
import com.healbe.healbesdk.business_api.user.data.HealbeSessionState
import com.healbe.healbesdk.business_api.user_storage.entity.GoBeVersion
import com.healbe.healbesdk.business_api.user_storage.entity.HealbeDevice
import com.healbe.healbesdk.device_api.ClientState
import com.healbe.simple_example.databinding.ActivitySimpleBinding
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit

// show data gathered from the smart band or the server
class SimpleActivity : ComponentActivity() {
    private val unsubscribeOnDestroy = CompositeDisposable()

    private lateinit var binding: ActivitySimpleBinding

    private val requestPermissionLauncher: ActivityResultLauncher<Array<String>> =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { result ->
            checkStateAndPrepareSdk(result.all { (_, isGranted) -> isGranted })
        }

    private val requestEnableBluetooth =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                checkStateAndPrepareSdk()
            } else {
                showBluetoothNeededDialog()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (BuildConfig.DEBUG && Timber.treeCount == 0) {
            Timber.plant(Timber.DebugTree())
        }

        binding = ActivitySimpleBinding.inflate(LayoutInflater.from(this))
        with (binding) {
            setContentView(root)

            progress.isVisible = true
            connectionState.isVisible = true
            hydration.isVisible = false
            stress.isVisible = false
            pulse.isVisible = false
            synchronizationState.isVisible = false
        }

        //check permissions, turn on bluetooth and run
        checkStateAndPrepareSdk()
    }

    override fun onDestroy() {
        // clear all subscribers
        unsubscribeOnDestroy.clear()
        super.onDestroy()
    }

    private fun showBluetoothPermissionsNeededDialog() {
        MaterialAlertDialogBuilder(this, R.style.AlertDialogCustom)
            .setMessage(R.string.warn_blu_permission)
            .setPositiveButton(getString(R.string.ok)) { _: DialogInterface?, _: Int ->
                Bluetooth.requestBluetoothConnectionPermissions(this, requestPermissionLauncher)
            }
            .setNegativeButton(getString(R.string.logout)) { _: DialogInterface?, _: Int -> finish() }
            .setCancelable(false)
            .show()
    }

    private fun showBluetoothNeededDialog() {
        MaterialAlertDialogBuilder(this, R.style.AlertDialogCustom)
            .setMessage(R.string.warn_blu)
            .setPositiveButton(getString(R.string.ok)) { _: DialogInterface?, _: Int ->
                if (Bluetooth.isBluetoothConnectPermissionGranted(this)) {
                    Bluetooth.requestEnableBluetooth(requestEnableBluetooth)
                } else {
                    showBluetoothPermissionsNeededDialog()
                }
            }
            .setNegativeButton(getString(R.string.logout)) { _: DialogInterface?, _: Int -> finish() }
            .setCancelable(false)
            .show()
    }

    private fun checkStateAndPrepareSdk(areAllPermissionsGranted: Boolean? = null) {
        // We need only connection permissions because in this sample we won't scan for the HEALBE GoBe
        // smart band but rather set it's MAC-address and PIN-code in DEVICE constant.
        if (!(areAllPermissionsGranted ?: Bluetooth.isBluetoothConnectPermissionGranted(this))) {
            showBluetoothPermissionsNeededDialog()
        } else if (!Bluetooth.isOn(this)) {
            showBluetoothNeededDialog()
        } else {
            runChain()
        }
    }

    private fun runChain() {
        unsubscribeOnDestroy.add(
            initSdk()
                .subscribeOn(Schedulers.io())
                .andThen(prepareSession())
                .andThen(login())
                .andThen(setupDevice())
                .andThen(connect())
                .andThen(observeSimpleData()) // observe on main thread to able to update UI
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { simpleData: SimpleData -> binding.showSimpleData(simpleData) },
                    { e: Throwable -> binding.showError(e) }
                )
        )
    }

    private fun initSdk(): Completable = Completable.defer {
        HealbeSdk.init(applicationContext).observeOn(Schedulers.io())
    }

    private fun prepareSession(): Completable = Completable.defer {
        HealbeSdk.get().USER.prepareSession().ignoreElement().observeOn(Schedulers.io())
    }

    private fun login(): Completable = Completable.defer {
        HealbeSdk.get().USER.login(USER_EMAIL, USER_PASSWORD)
            .flatMapCompletable { sessionState: HealbeSessionState ->
                if (HealbeSessionState.isUserValid(sessionState)) {
                    Completable.complete()
                } else {
                    Completable.error(RuntimeException("user invalid"))
                }
            }
            .observeOn(Schedulers.io())
    }

    private fun setupDevice(): Completable = Completable.defer {
        HealbeSdk.get().GOBE.set(DEVICE).observeOn(Schedulers.io())
    }

    private fun connect(): Completable = Completable.defer {
        // begin connection to the device that was set previously
        HealbeSdk.get().GOBE.connect()
            // observe connection states
            .andThen(HealbeSdk.get().GOBE.observeConnectionState())
            // show state
            .doOnNext { clientState: ClientState ->
                binding.connectionState.post {
                    binding.connectionState.text = clientState.toString()
                }
            }
            // wait for READY state
            .skipWhile { clientState: ClientState -> clientState != ClientState.READY }
            .firstElement() // we can ignore state because of filter
            .ignoreElement()
    }

    private fun observeSimpleData(): Flowable<SimpleData> = Flowable.defer {
        Flowable.combineLatest(
            HealbeSdk.get().TASKS.observeTasksState().toFlowable(BackpressureStrategy.LATEST),
            HealbeSdk.get().TASKS.observeSyncProgress().toFlowable(BackpressureStrategy.LATEST),
            HealbeSdk.get().HEALTH_DATA.getHydrationSummary(0),
            HealbeSdk.get().HEALTH_DATA.getStressSummary(0),
            HealbeSdk.get().HEALTH_DATA.getHeartData(0)
        ) { tasksState: TasksState, syncProgress: Int,
            hydrationSummaryContainer: Container<HydrationSummary>,
            stressSummaryContainer: Container<StressSummary>,
            heartDataList: List<HeartData> ->

            SimpleData(
                tasksState, syncProgress,
                hydrationSummaryContainer, stressSummaryContainer, heartDataList
            )
        }
    }

    private fun ActivitySimpleBinding.showSimpleData(simpleData: SimpleData) {
        progress.isVisible = false
        connectionState.isVisible = false
        hydration.isVisible = true
        stress.isVisible = true
        pulse.isVisible = true
        synchronizationState.isVisible = true

        hydration.text = simpleData.hydrationSummaryContainer.get()?.let { hydrationSummary ->
            val dehydrationDuration = hydrationSummary.getDehydrationDuration(TimeUnit.MINUTES)
            val normalHydrationDuration =
                hydrationSummary.getNormalHydrationDuration(TimeUnit.MINUTES)
            "Average hydration: ${if (dehydrationDuration > normalHydrationDuration) "Low" else "Normal"}"
        }
            ?: "Average hydration: no data yet"

        stress.text = simpleData.stressSummaryContainer.get()?.let {stressSummary ->
            "Average stress: ${stressSummary.averageState.toString().lowercase(getCurrentLocale())}"
        }
            ?: "Average stress: no data yet"

        pulse.text = simpleData.heartDataList.maxOfOrNull { it.maximumHeartRate }?.let {
            "Max pulse: $it bpm"
        }
            ?: "Max pulse: no data yet"

        synchronizationState.text = when (simpleData.tasksState) {
            TasksState.STOPPED -> "Synchronization stopped"
            TasksState.OPERATIVE -> "Synchronization has complete and will be repeated soon"
            TasksState.SYNC -> "Synchronization in progress: ${simpleData.syncProgress}%"
        }
    }

    private fun ActivitySimpleBinding.showError(e: Throwable) {
        progress.isVisible = false
        hydration.isVisible = false
        stress.isVisible = false
        pulse.isVisible = false
        synchronizationState.isVisible = false
        connectionState.isVisible = true
        connectionState.text = e.message
    }

    private data class SimpleData(
        val tasksState: TasksState, val syncProgress: Int,
        val hydrationSummaryContainer: Container<HydrationSummary>,
        val stressSummaryContainer: Container<StressSummary>,
        val heartDataList: List<HeartData>
    )

    companion object {
        private const val USER_EMAIL = "user@healbe.com"
        private const val USER_PASSWORD = "password"

        // Update MAC address and PIN-code to connect to your GoBe
        private val DEVICE = HealbeDevice.EMPTY.copy(
            name = "deviceName",
            mac = "00:00:00:00:00:00",
            pin = "000000",
            version = GoBeVersion.GOBE_3_0
        )
    }
}